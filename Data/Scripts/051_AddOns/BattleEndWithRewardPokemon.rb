#===============================================================================
# Battle End with Reward Pokémon
#===============================================================================
# By: TechnoHornist
#===============================================================================
# Version 3.1a (Compatible with Infinite Fusion)
#===============================================================================
# Purpose
#
# The purpose of this plugin is to alter the battle logic to give the player a
# free Pokémon whenever the player successfully captures a wild Pokémon in
# non-Contest settings.  This includes regular wild encounters and Safari Zone
# wild encounters only.
# 
# This plugin does the following:
# 1. Adds the new function pbStoreArceusRewardPokemon to the Utilities script group.
# 2. Adds the new function pbAddArceusRewardPokemon to the Utilities script group.
# 3. Adds the new function pbGetRandomArceusRewardPokemon to the Utilites script group.
# 4. Overrides the vanilla pbEndOfBattle function in the PokeBattle_Battle class
#    with a version that includes a call to the pbAddArceusRewardPokemon function.
# 5. Overrides the vanilla pbStartBattle function in the PokeBattle_SafariZone class
#    with a version that includes a call to the pbAddArceusRewardPokemon function.
#===============================================================================
# Customize Reward Pokémon
#
# To customize the Reward Pokémon (species, level, etc.), please refer to the
# section "Toggle Switches and Options" and the descriptions in it.
#
#===============================================================================
# How to Install/Enable
# 
# Copy and paste this file into the following location in your Infinite Fusion
# game directory: /Data/Scripts/051_AddOns
#
# To enable this plugin, set the value of REWARD_ENABLED_SWITCH to true
#===============================================================================
# How to Disable/Uninstall
# 
# To disable this plugin without removing it from your game directory,
# set the value of REWARD_ENABLED_SWITCH to false
#
# To remove this plugin completely, delete this file from the
# /Data/Scripts/051_AddOns folder in your Infinite Fusion game directory.
#===============================================================================
# WARNING
# 
# DO NOT MODIFY AND/OR DELETE ANY OTHER SCRIPTS INSIDE YOUR GAME'S DATA FOLDER.
# THEY ARE REQUIRED IN ORDER FOR THE GAME TO RUN PROPERLY.
# IF YOU DO NOT KNOW WHAT YOU ARE DOING, MODIFYING AND/OR DELETING ANY SCRIPTS
# MAY CAUSE THE GAME TO NOT RUN PROPERLY.
#===============================================================================

#===============================================================================
# Toggle Switches and Options
#
# The values of the variables in all capital letters described here can be
# altered to change this plugin's effects.  The variables and their effects are
# described below.
#
# REWARD_ENABLED_SWITCH
# true = Reward Pokémon enabled
# false = Reward Pokémon disabled
#
# STATIC_LEVEL_SWITCH
# true = Reward Pokémon's level is equal to the value of REWARD_POKEMON_LEVEL
# false = Reward Pokémon's level is equal to the level of the player's first
#         Pokémon in the party.
#
# CUSTOM_ATTRIBUTES_SWITCH
# true = Apply custom attributes and moves to the Reward Pokémon
# false = Do not apply custom attributes and moves to the Reward Pokémon
# NOTE: This switch's state is only recognized if the Reward Pokémon is Quagsire
#
# REWARD_POKEMON_SPECIES
# An array of Reward Pokémon names (all names must be in all capital letters with colon prefixes)
# NOTE 1: Names must be valid unfused Pokémon that exist in Infinite Fusion
# NOTE 2: Fused Pokémon names DO NOT WORK
# NOTE 3: To receive only one kind of Reward Pokémon, add only one name to the array
# NOTE 4: To increase the chance of a given Pokémon appearing as a reward over the others,
#         add its name more than once to the array
#
# REWARD_POKEMON_LEVEL
# Reward Pokémon's level is set to this value when STATIC_LEVEL_SWITCH is true
# NOTE: Value must be an integer between 1 and 100 inclusive
#===============================================================================
REWARD_ENABLED_SWITCH = true
STATIC_LEVEL_SWITCH = false
CUSTOM_ATTRIBUTES_SWITCH = true

REWARD_POKEMON_SPECIES = [:QUAGSIRE]
REWARD_POKEMON_LEVEL = 5

#===============================================================================
# Set plugin credits if not already defined in the Plugin Manager
#===============================================================================
 if defined?(PluginManager) && !PluginManager.installed?("Battle End with Reward Pokémon")
   PluginManager.register({
                            :name    => "Battle End with Reward Pokémon",
                            :version => "3.0a",
                            :credits => "TechnoHornist"
                          })
 end
 
 if defined?(PluginManager) && !PluginManager.installed?("Additional Graphics and Sprites")
   PluginManager.register({
                            :name    => "Additional Graphics and Sprites",
                            :version => "1.0",
                            :credits => "TechnoHornist"
                          })
 end
 
 if defined?(PluginManager) && !PluginManager.installed?("Additional Music and Sound Effects")
   PluginManager.register({
                            :name    => "Additional Music and Sound Effects",
                            :version => "X/Y",
                            :credits => "ENLS's Pre-Looped Music Library"
                          })
 end

#===============================================================================
# Get randomly-seleced index based on the number of Pokémon in the reward array
#===============================================================================
def pbGetRandomArceusRewardPokemon(pkmnList)
	arrayLength = pkmnList.length()
	indexToUse = rand(arrayLength)
	return REWARD_POKEMON_SPECIES[indexToUse]
end

#===============================================================================
# Store Pokémon reward from Arceus
#===============================================================================
def pbStoreArceusRewardPokemon(pkmn)
  if pbBoxesFull?
    pbMessage(_INTL("There's no more room for Pokémon!\1"))
    pbMessage(_INTL("The Pokémon Boxes are full and can't accept any more!"))
    return
  end
  $Trainer.pokedex.set_seen(pkmn.species)
  $Trainer.pokedex.set_owned(pkmn.species)
  pbStorePokemon(pkmn)
end

#===============================================================================
# Give Pokémon reward from Arceus to the player (will send to storage if party is full)
#===============================================================================
  def pbAddArceusRewardPokemon(pkmn, level = 1, see_form = true)
    return false if !pkmn
  if pbBoxesFull?
    pbMessage(_INTL("There's no more room for Pokémon!\1"))
    pbMessage(_INTL("The Pokémon Boxes are full and can't accept any more!"))
    return false
  end
  pkmnName = pkmn
  pkmn = Pokemon.new(pkmn, level) if !pkmn.is_a?(Pokemon)
  species_name = pkmn.speciesName
  pkmn.poke_ball = :CHERISHBALL
  pkmn.item = :DNASPLICERS
  
  # Apply custom attributes and moves if the custom attributes switch is true and the Reward Pokémon is Quagsire
  if CUSTOM_ATTRIBUTES_SWITCH == true && pkmnName == :QUAGSIRE
	pkmn.ability = :PRESSURE
	pkmn.learn_move(:HYPERVOICE)
	pkmn.learn_move(:DISARMINGVOICE)
	pkmn.learn_move(:ROAROFTIME)
	pkmn.learn_move(:WATERPULSE)
  end
  
  pkmn.obtain_text = "Reward from Arceus"
  
  ## Unique text if Reward Pokémon is Arceus, regular text otherwise
  if pkmnName == :ARCEUS
	pbMessage(_INTL("{2} sent one of its clones to {1}!\\me[Battle capture success]\\wtnp[80]\1", $Trainer.name, species_name))
  else
	pbMessage(_INTL("{1} received {2} from Arceus!\\me[Battle capture success]\\wtnp[80]\1", $Trainer.name, species_name))
  end
  
  was_owned = $Trainer.owned?(pkmn.species)
  $Trainer.pokedex.set_seen(pkmn.species)
  $Trainer.pokedex.set_owned(pkmn.species)
  $Trainer.pokedex.register(pkmn) if see_form
  
  # Show Pokédex entry for new species if it hasn't been owned before
  if see_form && !was_owned && $Trainer.has_pokedex
	
	if pkmnName == :ARCEUS
		pbMessage(_INTL("{1} added its data to the Pokédex.", species_name))
	else
		pbMessage(_INTL("Arceus added {1}'s data to the Pokédex.", species_name))
	end
	
    $Trainer.pokedex.register_last_seen(pkmn)
    pbFadeOutIn {
      scene = PokemonPokedexInfo_Scene.new
      screen = PokemonPokedexInfoScreen.new(scene)
      screen.pbDexEntry(pkmn.species)
    }
  end

  # Add the reward Pokémon (nicknaming is handled in the pbEndOfBattle function)
  pbStoreArceusRewardPokemon(pkmn)
  return true
  end

#===============================================================================
# PokeBattle_Battle Class
#===============================================================================
class PokeBattle_Battle

#===============================================================================
# Define actions to be performed when a wild Pokémon or Trainer battle ends
#===============================================================================
  def pbEndOfBattle
    oldDecision = @decision
    @decision = 4 if @decision == 1 && wildBattle? && @caughtPokemon.length > 0
    case oldDecision
    ##### WIN #####
    when 1
      PBDebug.log("")
      PBDebug.log("***Player won***")
      if trainerBattle?
        @scene.pbTrainerBattleSuccess
        case @opponent.length
        when 1
          pbDisplayPaused(_INTL("You defeated {1}!",@opponent[0].full_name))
        when 2
          pbDisplayPaused(_INTL("You defeated {1} and {2}!",@opponent[0].full_name,
             @opponent[1].full_name))
        when 3
          pbDisplayPaused(_INTL("You defeated {1}, {2} and {3}!",@opponent[0].full_name,
             @opponent[1].full_name,@opponent[2].full_name))
        end
        @opponent.each_with_index do |_t,i|
          @scene.pbShowOpponent(i)
          msg = (@endSpeeches[i] && @endSpeeches[i]!="") ? @endSpeeches[i] : "..."
          pbDisplayPaused(msg.gsub(/\\[Pp][Nn]/,pbPlayer.name))
        end
      end
      # Gain money from winning a trainer battle, and from Pay Day
      pbGainMoney if @decision!=4
      # Hide remaining trainer
      @scene.pbShowOpponent(@opponent.length) if trainerBattle? && @caughtPokemon.length>0
      if $game_switches[AUTOSAVE_WIN_SWITCH]
        Kernel.tryAutosave()
      end
    ##### LOSE, DRAW #####
    when 2, 5
      PBDebug.log("")
      PBDebug.log("***Player lost***") if @decision==2
      PBDebug.log("***Player drew with opponent***") if @decision==5
      if @internalBattle
        pbDisplayPaused(_INTL("You have no more Pokémon that can fight!"))
        if trainerBattle?
          case @opponent.length
          when 1
            pbDisplayPaused(_INTL("You lost against {1}!",@opponent[0].full_name))
          when 2
            pbDisplayPaused(_INTL("You lost against {1} and {2}!",
               @opponent[0].full_name,@opponent[1].full_name))
          when 3
            pbDisplayPaused(_INTL("You lost against {1}, {2} and {3}!",
               @opponent[0].full_name,@opponent[1].full_name,@opponent[2].full_name))
          end
        end
        # Lose money from losing a battle
        pbLoseMoney
        pbDisplayPaused(_INTL("You blacked out!")) if !@canLose
      elsif @decision==2
        if @opponent
          @opponent.each_with_index do |_t,i|
            @scene.pbShowOpponent(i)
            msg = (@endSpeechesWin[i] && @endSpeechesWin[i]!="") ? @endSpeechesWin[i] : "..."
            pbDisplayPaused(msg.gsub(/\\[Pp][Nn]/,pbPlayer.name))
          end
        end
      end
    ##### CAUGHT WILD POKÉMON #####
    when 4
      @scene.pbWildBattleSuccess if !Settings::GAIN_EXP_FOR_CAPTURE
    end
    # Register captured Pokémon in the Pokédex, and store them
    pbRecordAndStoreCaughtPokemon
	
	#===============================================================================
	# Give player a Reward Pokémon on successful capture
    # Requested by Tom Fawkes on May 25, 2023
	#===============================================================================
	if REWARD_ENABLED_SWITCH == true
		rewardPokemonChosen = pbGetRandomArceusRewardPokemon(REWARD_POKEMON_SPECIES)
	
		if STATIC_LEVEL_SWITCH == true
			pbAddArceusRewardPokemon(rewardPokemonChosen, REWARD_POKEMON_LEVEL) if @decision == 4
		else
			pbAddArceusRewardPokemon(rewardPokemonChosen, $Trainer.party[0].level) if @decision == 4
		end
	end
	
	isRematch = $game_switches[SWITCH_IS_REMATCH]
    begin
    if isRematch
      if @opponent.is_a?(Array)
        for trainer in @opponent
          rematchId = getRematchId(trainer.name,trainer.trainer_type)
          incrNbRematches(rematchId)
        end
      else
        rematchId = getRematchId(@opponent.name,@opponent.trainer_type)
        incrNbRematches(rematchId)
      end
    end
    rescue
      $game_switches[SWITCH_IS_REMATCH]=false
    end
	
    # Collect Pay Day money in a wild battle that ended in a capture
    pbGainMoney if @decision == 4
    # Pass on Pokérus within the party
    if @internalBattle
      infected = []
      $Trainer.party.each_with_index do |pkmn, i|
        infected.push(i) if pkmn.pokerusStage == 1
      end
      infected.each do |idxParty|
        strain = $Trainer.party[idxParty].pokerusStrain
        if idxParty > 0 && $Trainer.party[idxParty - 1].pokerusStage == 0 && rand(3) == 0   # 33%
          $Trainer.party[idxParty - 1].givePokerus(strain)
        end
        if idxParty < $Trainer.party.length - 1 && $Trainer.party[idxParty + 1].pokerusStage == 0 && rand(3) == 0   # 33%
          $Trainer.party[idxParty + 1].givePokerus(strain)
        end
      end
    end
    # Clean up battle stuff
    @scene.pbEndBattle(@decision)
    @battlers.each do |b|
      next if !b
      pbCancelChoice(b.index)   # Restore unused items to Bag
      BattleHandlers.triggerAbilityOnSwitchOut(b.ability,b,true) if b.abilityActive?
    end
    pbParty(0).each_with_index do |pkmn,i|
      next if !pkmn
      @peer.pbOnLeavingBattle(self,pkmn,@usedInBattle[0][i],true)   # Reset form
      pkmn.item = @initialItems[0][i]
    end
    return @decision
  end
end

#===============================================================================
# PokeBattle_SafariZone Class
#===============================================================================
class PokeBattle_SafariZone

#===============================================================================
# Define actions to be performed when encountering a wild Pokémon inside the
# Safari Zone.
#===============================================================================
def pbStartBattle
    begin
      pkmn = @party2[0]
      self.pbPlayer.pokedex.register(pkmn)
      @scene.pbStartBattle(self)
      pbDisplayPaused(_INTL("Wild {1} appeared!",pkmn.name))
      @scene.pbSafariStart
      weather_data = GameData::BattleWeather.try_get(@weather)
      @scene.pbCommonAnimation(weather_data.animation) if weather_data
      safariBall = GameData::Item.get(:SAFARIBALL).id
      catch_rate = pkmn.species_data.catch_rate
      catchFactor  = (catch_rate*100)/1275
      catchFactor  = [[catchFactor,3].max,20].min
      escapeFactor = (pbEscapeRate(catch_rate)*100)/1275
      escapeFactor = [[escapeFactor,2].max,20].min
      loop do
        cmd = @scene.pbSafariCommandMenu(0)
        case cmd
        when 0   # Ball
          if pbBoxesFull?
            pbDisplay(_INTL("The boxes are full! You can't catch any more Pokémon!"))
            next
          end
          @ballCount -= 1
          @scene.pbRefresh
          rare = (catchFactor*1275)/100
          if safariBall
            pbThrowPokeBall(1,safariBall,rare,true)
            if @caughtPokemon.length>0
              pbRecordAndStoreCaughtPokemon
              @decision = 4

			  #===============================================================================
			  # Give player a Reward Pokémon on successful capture
			  # Requested by Tom Fawkes on May 25, 2023
			  #===============================================================================
			  if REWARD_ENABLED_SWITCH == true
				  rewardPokemonChosen = pbGetRandomArceusRewardPokemon(REWARD_POKEMON_SPECIES)
	
				  if STATIC_LEVEL_SWITCH == true
					  pbAddArceusRewardPokemon(rewardPokemonChosen, REWARD_POKEMON_LEVEL) if @decision == 4
				  else
					  pbAddArceusRewardPokemon(rewardPokemonChosen, $Trainer.party[0].level) if @decision == 4
				  end
			  end
			  
            end
          end
        when 1   # Bait
          pbDisplayBrief(_INTL("{1} threw some bait at the {2}!",self.pbPlayer.name,pkmn.name))
          @scene.pbThrowBait
          catchFactor  /= 2 if pbRandom(100)<90   # Harder to catch
          escapeFactor /= 2                       # Less likely to escape
        when 2   # Rock
          pbDisplayBrief(_INTL("{1} threw a rock at the {2}!",self.pbPlayer.name,pkmn.name))
          @scene.pbThrowRock
          catchFactor  *= 2                       # Easier to catch
          escapeFactor *= 2 if pbRandom(100)<90   # More likely to escape
        when 3   # Run
          pbSEPlay("Battle flee")
          pbDisplayPaused(_INTL("You got away safely!"))
          @decision = 3
        end
        catchFactor  = [[catchFactor,3].max,20].min
        escapeFactor = [[escapeFactor,2].max,20].min
        # End of round
        if @decision==0
          if @ballCount<=0
            pbDisplay(_INTL("PA: You have no Safari Balls left! Game over!"))
            @decision = 2
          elsif can_escape(pkmn, escapeFactor)
            pbSEPlay("Battle flee")
            pbDisplay(_INTL("{1} fled!",pkmn.name))
            @decision = 3
          elsif cmd==1   # Bait
            pbDisplay(_INTL("{1} is eating!",pkmn.name))
          elsif cmd==2   # Rock
            pbDisplay(_INTL("{1} is angry!",pkmn.name))
          else
            pbDisplay(_INTL("{1} is watching carefully!",pkmn.name))
          end
          # Weather continues
          weather_data = GameData::BattleWeather.try_get(@weather)
          @scene.pbCommonAnimation(weather_data.animation) if weather_data
        end
        break if @decision > 0
      end
      @scene.pbEndBattle(@decision)
    rescue BattleAbortedException
      @decision = 0
      @scene.pbEndBattle(@decision)
    end
    return @decision
  end
end