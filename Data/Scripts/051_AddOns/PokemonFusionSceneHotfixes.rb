#===============================================================================
# Pokémon Fusion Scene Hotfixes
#===============================================================================
# By: TechnoHornist
#===============================================================================
# Version 2.0
#===============================================================================
# Purpose
#
# The purpose of this plugin is to provide hotfixes to the PokemonFusionScene
# class and prevent the BGM from stopping altogether.
#
# Requires a file called "fusion.mp3" in the /Audio/ME folder.
# Overrides vanilla pbFusionScene in the file "048_Fusion/PokemonFusion.rb"
#===============================================================================

 if defined?(PluginManager) && !PluginManager.installed?("Pokémon Fusion Scene Hotfixes")
   PluginManager.register({
                            :name    => "Pokémon Fusion Scene Hotfixes",
                            :version => "2.0",
                            :credits => "TechnoHornist"
                          })
 end

class PokemonFusionScene
  def pbFusionScreen(cancancel = false, superSplicer = false, firstOptionSelected = false)
    metaplayer1 = SpriteMetafilePlayer.new(@metafile1, @sprites["rsprite1"])
    metaplayer2 = SpriteMetafilePlayer.new(@metafile2, @sprites["rsprite2"])
    metaplayer3 = SpriteMetafilePlayer.new(@metafile3, @sprites["rsprite3"])
    metaplayer4 = SpriteMetafilePlayer.new(@metafile4, @sprites["dnasplicer"])																		  
    metaplayer1.play
    metaplayer2.play
    metaplayer3.play

					 
    pbPlayCry(@pokemon)
	
	if @pokemon1.name == "Miltank" || @pokemon2.name == "Miltank"
	    Kernel.pbMessageDisplay(@sprites["msgwindow"],
                            _INTL("What are you doing?!  You've doomed us all!", @pokemon1.name))
	else
	    Kernel.pbMessageDisplay(@sprites["msgwindow"],
                            _INTL("The Pokémon are being fused!", @pokemon1.name))
	end

    Kernel.pbMessageWaitForInput(@sprites["msgwindow"], 100, true)
    pbPlayDecisionSE()
    oldstate = pbSaveSpriteState(@sprites["rsprite1"])
    oldstate2 = pbSaveSpriteState(@sprites["rsprite2"])
    oldstate3 = pbSaveSpriteState(@sprites["rsprite3"])

	pbMEPlay("fusion")

    canceled = false
    noMoves = false
    begin
      metaplayer1.update
      metaplayer2.update
      metaplayer3.update

      Graphics.update
      Input.update
      if Input.trigger?(Input::B) && Input.trigger?(Input::C)
        noMoves = true
        pbSEPlay("buzzer")
        Graphics.update
      end
    end while metaplayer1.playing? && metaplayer2.playing?
    if canceled

      pbPlayCancelSE()

      @pbEndScreen
      _INTL("Huh? The fusion was cancelled!")
    else
      frames = pbCryFrameLength(@newspecies)
				  
      pbPlayCry(@newspecies)
      frames.times do
        Graphics.update
      end
      pbMEPlay("Voltorb Flip Win")
      newSpecies = GameData::Species.get(@newspecies)
      newspeciesname = newSpecies.real_name
      oldspeciesname = GameData::Species.get(@pokemon1.species).real_name

      overlay = BitmapSprite.new(Graphics.width, Graphics.height, @viewport).bitmap

      sprite_bitmap = @sprites["rsprite2"].getBitmap
      drawSpriteCredits(sprite_bitmap.filename, sprite_bitmap.path, @viewport)
	  
	  if @pokemon1.name == "Miltank" || @pokemon2.name == "Miltank"
	        Kernel.pbMessageDisplay(@sprites["msgwindow"],
                              _INTL("\\se[]WHAT HAVE YOU DONE?! Your Pokémon were fused into {2}!\\wt[80]", @pokemon1.name, newspeciesname))
	  else
	        Kernel.pbMessageDisplay(@sprites["msgwindow"],
                              _INTL("\\se[]Congratulations! Your Pokémon were fused into {2}!\\wt[80]", @pokemon1.name, newspeciesname))
	  end
      #exp
      @pokemon1.exp_when_fused_head = @pokemon2.exp
      @pokemon1.exp_when_fused_body = @pokemon1.exp
      @pokemon1.exp_gained_since_fused = 0

      if @pokemon2.shiny?
        @pokemon1.head_shiny = true
      end
      if @pokemon1.shiny?
        @pokemon1.body_shiny = true
      end
      @pokemon1.debug_shiny = true if @pokemon1.debug_shiny || @pokemon2.debug_shiny

      setFusionIVs(superSplicer)
      #add to pokedex
      if !$Trainer.pokedex.owned?(newSpecies)
        $Trainer.pokedex.set_seen(newSpecies)
        $Trainer.pokedex.set_owned(newSpecies)
        Kernel.pbMessageDisplay(@sprites["msgwindow"],
                                _INTL("{1}'s data was added to the Pokédex", newspeciesname))
        @scene.pbShowPokedex(@newspecies)
      end
      overlay.dispose
      #first check if hidden ability
      # getAbilityList format: [[:ABILITY, index],...]
      # hiddenAbility1 = @pokemon1.ability == @pokemon1.getAbilityList[-1][0]
      # hiddenAbility2 = @pokemon2.ability == @pokemon2.getAbilityList[-1][0]

      #change species
	  ability1 = @pokemon1.ability
      ability2 = @pokemon2.ability

      @pokemon1.species = newSpecies
      if @pokemon2.egg? || @pokemon1.egg?
        @pokemon1.steps_to_hatch = @pokemon1.species_data.hatch_steps
      end

      pbChooseAbility(ability1,ability2)

      setFusionMoves(@pokemon1, @pokemon2, firstOptionSelected) if !noMoves

      removeItem = false
      if @pokemon2.isShiny? || @pokemon1.isShiny?
        @pokemon1.makeShiny
        if !(@pokemon1.debug_shiny  ||@pokemon2.debug_shiny)
          @pokemon1.natural_shiny = true if @pokemon2.natural_shiny
        end
      end

      #make it untraded, pour qu'on puisse le unfused après, même si un des 2 était traded
      @pokemon1.obtain_method = 0
      @pokemon1.owner = Pokemon::Owner.new_from_trainer($Trainer)

      pbSEPlay("Voltorb Flip Point")

      @pokemon1.name = newspeciesname if @pokemon1.name == oldspeciesname

      @pokemon1.level = setPokemonLevel(@pokemon1.level, @pokemon2.level, superSplicer)
      @pokemon1.calc_stats
      @pokemon1.obtain_method = 0
    end
  end
end