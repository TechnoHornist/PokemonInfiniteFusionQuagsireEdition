# Pokémon Infinite Fusion - Quagsire Edition
A TomFawkes-themed asset pack for Pokémon Infinite Fusion.

# Features

1. Custom graphics assets for the loading screen, title screen, new game intro, and Fighting Arena overworld sprite.
2. Cry audio file fixes for various Pokémon that have broken cry audio in vanilla Pokémon Infinite Fusion.
3. BGM audio file replacements for various areas in vanilla Pokémon Infinite Fusion that have no BGM playing due to missing files.
4. Custom cry audio for Quagsire.
5. "Battle End with Reward Pokémon" plugin script, configured by default to give the player a free level 5 Quagsire whenever the player catches a wild Pokémon.
6. Various customizable options to alter the plugin script's behavior.
7. Miscellaneous hotfixes.

# Installation
Before installing this asset pack, create a backup of your existing Pokémon Infinite Fusion game directory in case you want to revert to the vanilla game.

To install this asset pack, copy and paste the Audio, Data, and Graphics folders into the root directory of your Pokémon Infinite Fusion game folder.  When prompted, select "Replace the files in the destination."

# Compatibility

This asset pack is compatible with Pokémon Infinite Fusion v6.0 as of the current release.  The developer of this asset pack cannot guarantee compatibility with older versions or later versions.
